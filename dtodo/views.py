from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.context_processors import csrf

def login(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('login.html', c)

def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)
        return HttpResponseRedirect('/todo/')
    else:
        return HttpResponseRedirect('/accounts/invalid')

def loggedin(request):
    return render_to_response('loggedin.html',
                                {'full_name' : request.user.username})

def invalid_login(request):
    return render_to_response('invalid_login.html')

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')
    #return render_to_response('logout.html')

def home(request):
    return render_to_response('home.html')

def register(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('register.html', c)

def registration(request):
    username = request.POST.get('username', '')
    email = request.POST.get('email', '')
    password = request.POST.get('password', '')

    user = User.objects.create_user(username=username, email=email, password=password)
    user.save()
    return HttpResponseRedirect('/accounts/registered')

def registered(request):
    return render_to_response('registered.html')

def not_logged_in(request):
    return render_to_response('not_logged_in.html')
