from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dtodo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', 'dtodo.views.home'),
    url(r'^accounts/login/$', 'dtodo.views.login'),
    url(r'^accounts/auth/$', 'dtodo.views.auth_view'),
    url(r'^accounts/logout/$', 'dtodo.views.logout'),
    url(r'^accounts/loggedin/$', 'dtodo.views.loggedin'),
    url(r'^accounts/invalid/$', 'dtodo.views.invalid_login'),
    url(r'^accounts/register/$', 'dtodo.views.register'),
    url(r'^accounts/registration/$', 'dtodo.views.registration'),
    url(r'^accounts/registered/$', 'dtodo.views.registered'),
    url(r'^accounts/not_logged_in/$', 'dtodo.views.not_logged_in'),

    url(r'^todo/',include('todo.urls', namespace='todo')),

)
