from django.conf.urls import patterns, include, url
from todo import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dtodo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^$', 'todo.views.todo_view'),
    url(r'^$', views.TaskListView.as_view()),

)
