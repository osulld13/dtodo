from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from models import Task
from django.views import generic


class TaskListView(generic.ListView):
    template_name = 'todo/todo_view.html'
    context_object_name = 'task_list'

    def get_queryset(self):
        task_list = filter(lambda x: x.owner == self.request.user, Task.objects.all())
        return task_list
