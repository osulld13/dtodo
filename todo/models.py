from django.db import models
from django.contrib.auth.models import User

class Task(models.Model):
    name = models.CharField(max_length = 200)
    description = models.CharField(max_length = 1000)
    created = models.DateTimeField(auto_now_add = True)
    due = models.DateTimeField()
    owner = models.ForeignKey(User)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["due"]
